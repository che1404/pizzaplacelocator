//
//  PizzaPlaceLocatorNotifications.h
//  PizzaPlaceLocator
//
//  Created by Roberto Garrido Martín on 17/6/15.
//  Copyright (c) 2015 Robero Garrido. All rights reserved.
//

#ifndef PizzaPlaceLocator_PizzaPlaceLocatorNotifications_h
#define PizzaPlaceLocator_PizzaPlaceLocatorNotifications_h

#define NEW_PIZZA_PLACES_RECEIVED    @"NewPizzaPlacesReceived"

#endif
