//
//  ViewControllerDetailsPizzaPlace.m
//  PizzaPlaceLocator
//
//  Created by Roberto Garrido Martín on 17/6/15.
//  Copyright (c) 2015 Robero Garrido. All rights reserved.
//

#import "ViewControllerDetailsPizzaPlace.h"

@interface ViewControllerDetailsPizzaPlace ()

@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *labelDescription;

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* description;

@end

@implementation ViewControllerDetailsPizzaPlace

@synthesize name = _name;
@synthesize description = _description;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.labelName.text = self.name;
    self.labelDescription.text = self.description;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupWithName:(NSString*)name andDescription:(NSString*)description {
    self.name = name;
    self.description = description;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
