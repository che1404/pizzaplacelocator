//
//  TableViewControllerPizzaPlaces.m
//  PizzaPlaceLocator
//
//  Created by Roberto Garrido Martín on 17/6/15.
//  Copyright (c) 2015 Robero Garrido. All rights reserved.
//

#import "TableViewControllerPizzaPlaces.h"
#import "TableViewCellPizzaPlace.h"
#import "PizzaPlace.h"
#import "ViewControllerDetailsPizzaPlace.h"
#import "PizzaPlaceLocatorNotifications.h"

#import <MagicalRecord/MagicalRecord.h>

@interface TableViewControllerPizzaPlaces ()

@property (strong, nonatomic) NSArray* items;

@end

@implementation TableViewControllerPizzaPlaces

@synthesize items = _items;

- (NSArray*) items {
    if (!_items) {
        _items = [NSArray new];
    }
    
    return _items;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:NEW_PIZZA_PLACES_RECEIVED object:nil];
}

-(void)updateUI {
    _items = [PizzaPlace MR_findAll];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TableViewCellPizzaPlace *cell = [tableView dequeueReusableCellWithIdentifier: @"pizzaPlaceCell"];
    
    if (!cell) {
        [tableView registerNib: [UINib nibWithNibName:@"TableViewCellPizzaPlace" bundle:nil] forCellReuseIdentifier:@"pizzaPlaceCell"];
        cell = [tableView dequeueReusableCellWithIdentifier: @"pizzaPlaceCell"];
    }

    PizzaPlace* pizzaPlace = [self.items objectAtIndex:indexPath.row];
    cell.labelPizzaPlaceName.text = pizzaPlace.name;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(TableViewCellPizzaPlace *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PizzaPlace* pizzaPlace = [self.items objectAtIndex:indexPath.row];
    cell.labelPizzaPlaceName.text = pizzaPlace.name;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PizzaPlace* selectedPizzaPlace = [self.items objectAtIndex:indexPath.row];
    
    ViewControllerDetailsPizzaPlace* pizzaPlaceDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pizzaPlaceDetailsViewController"];
    
    [pizzaPlaceDetailsViewController setupWithName:selectedPizzaPlace.name andDescription:selectedPizzaPlace.desc];
    
    [self.navigationController pushViewController:pizzaPlaceDetailsViewController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
