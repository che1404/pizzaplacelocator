//
//  TableViewCellPizzaPlace.h
//  PizzaPlaceLocator
//
//  Created by Roberto Garrido Martín on 17/6/15.
//  Copyright (c) 2015 Robero Garrido. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellPizzaPlace : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelPizzaPlaceName;

@end
