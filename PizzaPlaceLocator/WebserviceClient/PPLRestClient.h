//
//  PPLRestClient.h
//  PizzaPlaceLocator
//
//  Created by Roberto Garrido Martín on 17/6/15.
//  Copyright (c) 2015 Robero Garrido. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PPLRestClient : NSObject<CLLocationManagerDelegate>

-(void)requestFiveNearestPizzaPlacesWithCompletionBlock:(void (^)(NSArray* venuesArray, NSError *error))completionBlock;

@end
