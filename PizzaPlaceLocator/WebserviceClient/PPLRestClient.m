//
//  PPLRestClient.m
//  PizzaPlaceLocator
//
//  Created by Roberto Garrido Martín on 17/6/15.
//  Copyright (c) 2015 Robero Garrido. All rights reserved.
//

#import "PPLRestClient.h"

#import <AFNetworking/AFNetworking.h>

static NSString * const BaseURLString = @"https://api.foursquare.com/v2/venues/";
static NSString * const FoursquareClientID = @"SVDU221KBRGHLSMOQ4NMWXT30J43FSLI4U3AUSBXFAEVA5S0";
static NSString * const FoursquareSecretID = @"YJB3XWQODPQA5DKOO1VRRVVJ5UA440TVVSDCJWG3HXOGV4XN";

@interface PPLRestClient()
{
    CLLocationManager* locationManager;
}
@end

@implementation PPLRestClient

-(void)requestFiveNearestPizzaPlacesWithCompletionBlock:(void (^)(NSArray* venuesArray, NSError *error))completionBlock {
    
    if ([CLLocationManager locationServicesEnabled]) {
        self->locationManager = [[CLLocationManager alloc]init];
        self->locationManager.delegate = self;
        self->locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        if ([self->locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self->locationManager requestWhenInUseAuthorization];
        }
        [self->locationManager startUpdatingLocation];
    }
    
    // https://api.foursquare.com/v2/venues/search?client_id=SVDU221KBRGHLSMOQ4NMWXT30J43FSLI4U3AUSBXFAEVA5S0&client_secret=YJB3XWQODPQA5DKOO1VRRVVJ5UA440TVVSDCJWG3HXOGV4XN&limit=5&query=pizza&v=20150617&near=newyork
    
    NSString *route = [NSString stringWithFormat:@"%@search", BaseURLString];
    NSDictionary *parameters = @{@"client_id": FoursquareClientID,
                                 @"client_secret": FoursquareSecretID,
                                 @"limit": @5,
                                 @"query": @"pizza",
                                 @"v": @"20150617",
                                 @"near": @"newyork"};
    

    
    NSMutableURLRequest* request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:route parameters:parameters error:nil];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *aData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (!error && aData != nil) {
        
        NSError *serializationError;
        NSMutableDictionary* jsonReturn = [[NSMutableDictionary alloc]init];
        jsonReturn = (NSMutableDictionary*)[NSJSONSerialization JSONObjectWithData:aData options:kNilOptions error:&serializationError];
        
        if (!serializationError && jsonReturn) {
            NSDictionary* responseDictionary = [jsonReturn objectForKey:@"response"];
            NSArray* venuesArray = [responseDictionary objectForKey:@"venues"];
            
            if ([[[jsonReturn objectForKey:@"meta"] objectForKey:@"code"] intValue] == 500) {
                NSLog(@"Error from Foursquare servers");
                completionBlock(nil, nil);
            }
            else
                completionBlock(venuesArray, nil);
        }
        else {
            completionBlock(nil, serializationError);
        }
        
    }
    else {
        completionBlock(nil, error);
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"%@", [locations lastObject]);
}



@end
