// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PizzaPlace.h instead.

@import CoreData;

extern const struct PizzaPlaceAttributes {
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *pizzaPlaceID;
} PizzaPlaceAttributes;

@interface PizzaPlaceID : NSManagedObjectID {}
@end

@interface _PizzaPlace : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PizzaPlaceID* objectID;

@property (nonatomic, strong) NSString* desc;

//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* pizzaPlaceID;

//- (BOOL)validatePizzaPlaceID:(id*)value_ error:(NSError**)error_;

@end

@interface _PizzaPlace (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitivePizzaPlaceID;
- (void)setPrimitivePizzaPlaceID:(NSString*)value;

@end
