// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PizzaPlace.m instead.

#import "_PizzaPlace.h"

const struct PizzaPlaceAttributes PizzaPlaceAttributes = {
	.desc = @"desc",
	.name = @"name",
	.pizzaPlaceID = @"pizzaPlaceID",
};

@implementation PizzaPlaceID
@end

@implementation _PizzaPlace

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PizzaPlace" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PizzaPlace";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PizzaPlace" inManagedObjectContext:moc_];
}

- (PizzaPlaceID*)objectID {
	return (PizzaPlaceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic desc;

@dynamic name;

@dynamic pizzaPlaceID;

@end

