//
//  AppDelegate.m
//  PizzaPlaceLocator
//
//  Created by Roberto Garrido Martín on 17/6/15.
//  Copyright (c) 2015 Robero Garrido. All rights reserved.
//

#import "AppDelegate.h"
#import "PPLRestClient.h"
#import "PizzaPlace.h"
#import "PizzaPlaceLocatorNotifications.h"

#import <MagicalRecord/MagicalRecord.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"PizzaPlaceLocator.sqlite"];
    
    PPLRestClient* client = [PPLRestClient new];
    [client requestFiveNearestPizzaPlacesWithCompletionBlock:^(NSArray *venuesArray, NSError *error) {
        if (!error && venuesArray != nil) {
            [self processVenues:venuesArray];
        }
        else {
            //TODO: Log error
        }
        
    }];
    return YES;
}

- (void) processVenues:(NSArray*)venuesArray {
    
    // TODO: Improve the serialization strategy
    // For now, truncate all the instances in the database
    [PizzaPlace MR_truncateAll];
    
    for (NSDictionary* venueDictionary in venuesArray) {
        [self processVenue:venueDictionary];
    }
}

-(void) processVenue:(NSDictionary*)venueDictionary {

    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        PizzaPlace* pizzaPlace = [PizzaPlace MR_createEntityInContext:localContext];
        pizzaPlace.name = [venueDictionary valueForKey:@"name"];
        pizzaPlace.pizzaPlaceID = [venueDictionary valueForKey:@"id"];
        
        NSMutableString* desc = [NSMutableString stringWithString:@"Another pizza place"];
        NSArray* categories = [venueDictionary valueForKey:@"categories"];
        if (categories.count > 0) {
            NSDictionary* categoryDictionary = [categories objectAtIndex:0];
            desc = [categoryDictionary objectForKey:@"name"];
        }
        pizzaPlace.desc = desc;
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        
        if (contextDidSave) {
        
            [[NSNotificationCenter defaultCenter] postNotificationName:NEW_PIZZA_PLACES_RECEIVED object:self];
        }
        else {
            // TODO: Handle error
        }
        
    }];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
    [MagicalRecord cleanUp];
}


@end
